(function ($, _) {
    var RM = Confluence.Roadmap = Confluence.Roadmap || {};

    RM.DateUtilities = {
        //1000 * 60 * 60 * 24
        MILLISECONDS_A_DAY: 86400000,

        //1000 * 60 * 60 * 24 * 7
        MILLISECONDS_A_WEEK: 604800000,

        /**
         * Try to parse a string to a date. If can not parse, return null.
         * @param strDate
         * @return {Moment}
         */
        parseDate: function(strDate) {
            var date = moment(strDate, Roadmap.SHORT_DATE_FORMAT, true);
            if (date.isValid()) {
                return date;
            }

            date = moment(strDate, Roadmap.TWO_YY_SHORT_DATE_FORMAT, true);

            return date.isValid() ? date : null;
        },

        /**
         * Get date string from date
         *
         * @param {date} date value
         */
        getDateStringFromDate: function(date, dateFormat) {
            if (dateFormat) {
                return moment(date).format(dateFormat);
            }

            return moment(date).format(Roadmap.DATE_FORMAT);
        },

        /**
         * Get date from date string
         *
         * @param {dateString} date string with Roadmap date format
         * @return -1 if date string is not a date;
         */
        getDateFromDateString: function(dateString, dateFormat) {
            return moment(dateString, dateFormat || Roadmap.DATE_FORMAT).toDate();
        },

        /**
         * Get number of months within a period
         *
         * @param {startDate}
         * @param {endDate}
         * @return number of months within the period
         */
        getNumberOfMonths: function(startTime, endTime) {
            var mStartDate = moment(startTime),
                mEndDate = moment(endTime);

            return mEndDate.diff(mStartDate, 'months');
        },

        getMilisecondsInMonth: function(date) {
            return moment(date).daysInMonth() * RM.DateUtilities.MILLISECONDS_A_DAY;
        },

        getMilisecondsFromStartMonth: function(date) {
            return date.getTime() - moment(date).startOf('month').valueOf();
        },

        /**
         * calculate duration information of a moment date
         *
         * @param {Moment} mDate - moment date
         * @returns {Object} duration information object:
         *                      totalMillisOfMonth: total milliseconds of month get from date.
         *                      millisFromStartMonth: milliseconds from the month's beginning date to date.
         *                      millisRemainingOfMonth: milliseconds from date to the month's ending date.
         *                      durationOfMonth: month's duration from the month's beginning date to date.
         *                      remainingDurationOfMonth: month's duration from date to the month's ending date.
         */
        calculateDurationInformation: function(mDate) {
            var totalMillisOfMonth = RM.DateUtilities.getMilisecondsInMonth(mDate.toDate());
            var millisFromStartMonth = RM.DateUtilities.getMilisecondsFromStartMonth(mDate.toDate());
            var millisRemainingOfMonth = totalMillisOfMonth - millisFromStartMonth;

            return {
                totalMillisOfMonth: totalMillisOfMonth,
                millisFromStartMonth: millisFromStartMonth,
                millisRemainingOfMonth: millisRemainingOfMonth,
                durationOfMonth: millisFromStartMonth / totalMillisOfMonth,
                remainingDurationOfMonth: millisRemainingOfMonth / totalMillisOfMonth
            };
        },

        /**
         * Get week duration by a start date and month duration.
         *
         * @param {Date} startDate - start date native JS object.
         * @param {number} monthDuration - can be a floated number.
         * @returns {number} week duration, can be floated number with 2 decimal digits.
         */
        convertToWeekDuration: function(startDate, monthDuration) {
            var mStartDate = moment(startDate);
            var mEndDate = mStartDate.clone();

            var startDateDurationInformation = RM.DateUtilities.calculateDurationInformation(mStartDate);

            if (monthDuration <= startDateDurationInformation.remainingDurationOfMonth) {
                var millisDuration = monthDuration * startDateDurationInformation.totalMillisOfMonth;
                mEndDate = mEndDate.add(millisDuration, 'ms');
            } else {
                mEndDate = mEndDate.add(startDateDurationInformation.millisRemainingOfMonth, 'ms');
                monthDuration = monthDuration - startDateDurationInformation.remainingDurationOfMonth;
                var monthCount = Math.floor(monthDuration);
                if (monthCount > 0) {
                    mEndDate = mEndDate.add(monthCount, 'months');
                }

                var remainingDuration = monthDuration % 1;
                var totalMillisOfEndMonth = RM.DateUtilities.getMilisecondsInMonth(mEndDate.toDate());
                var remainingMillis = remainingDuration * totalMillisOfEndMonth;
                mEndDate.add(remainingMillis, 'ms');
            }

            return mEndDate.diff(mStartDate, 'weeks', true);
        },

        /**
         * Get month duration by a start date and week duration.
         *
         * @param {Date} startDate - start date native JS object
         * @param {number} weekDuration
         * @returns {number} month duration, can be floated number with 2 decimal digits.
         */
        convertToMonthDuration: function(startDate, weekDuration) {
            var mStartDate = moment(startDate);

            var millisDuration = weekDuration * RM.DateUtilities.MILLISECONDS_A_WEEK;
            var mEndDate = mStartDate.clone().add(millisDuration, 'ms');

            var monthCount = mEndDate.get('year') * 12 + mEndDate.get('month');
            monthCount = monthCount - (mStartDate.get('year') * 12 + mStartDate.get('month'));

            var startDateDurationInformation = RM.DateUtilities.calculateDurationInformation(mStartDate);

            if (monthCount === 0) {
                return millisDuration / startDateDurationInformation.totalMillisOfMonth;
            } else {
                var monthDuration = startDateDurationInformation.remainingDurationOfMonth;

                var endDateDurationInformation = RM.DateUtilities.calculateDurationInformation(mEndDate);
                monthDuration = monthDuration + endDateDurationInformation.durationOfMonth;

                monthDuration = monthDuration + (monthCount - 1);
                return monthDuration;
            }
        }
    };

    RM.FieldUtilities = {
        /**
         * Fix some issues for AUI date picker
         *
         * @param {$container} find date pickers in container
         */
        fixDatePickerFields: function($container) {
            // fix some issues relate to AUI datepicker by apply datepicker-patch.css
            // fix datepicker doesn't fire change when AUI versions which < 5.4.5
            var $datepickers = $container.find('input[data-aui-dp-uuid]');
            $datepickers.each(function(index, element) {
                var $dp = $(element),
                    uuid = $dp.attr('data-aui-dp-uuid');
                $dp.on('click focus', function() {
                    var $dpPopup = $('[data-aui-dp-popup-uuid=' + uuid + ']');
                    if (AJS.version <= '5.4.5') {
                        var defaultOnSelectHandler = $dpPopup.datepicker('option', 'onSelect');
                        $dpPopup.datepicker('option', 'onSelect', function (dateText, inst) {
                            defaultOnSelectHandler(dateText, inst);
                            $dp.change();
                        });
                    }
                    $dpPopup.parents('.aui-inline-dialog').addClass('datepicker-roadmap-patch');
                });
            });
        }
    };
}(AJS.$, window._));