Confluence.RoadmapLink = (function($, _) {
        Roadmap.trackingBars = [];
        /**
         *  linkParam = {
         *   id: roadmapData.id,
         *   roadmapHash: roadmapData.hash,
         *   version: roadmapData.version,
         *   roadmapBarId: this.options.bar.id
            }
         */
        function goToCreateLinkPage(linkParam, callback) {
            // this param decide the parent page after creating a page.
            // Work: 'Blank-page', 'Decision', 'Meeting notes', 'Retrospective', 'Troubleshooting article', 'Product requirement', etc...
            // not work: 'How-to-article', 'File list', 'JIRA report' : the param alway be override with null value
            var pageParam={};
            // there is some cases without pageId, (blogpost list), 
            if (parseInt(linkParam.pageId) <= 0) {
                linkParam.parentPageId = AJS.Meta.get('parent-page-id');
            }

            var barParamContext = $.param(linkParam);
            window.open(AJS.contextPath() + '/plugins/roadmap/create-page.action?' + barParamContext, '_blank');

            putLinkToPending(linkParam.roadmapBarId).done(function(data) {
                Roadmap.trackingBars.push(linkParam.roadmapBarId);
            });

            $(window).off('focus');
            $(window).on('focus', function() {
                if (!Roadmap.trackingBars.length) {
                    $(window).off('focus');
                }
                _.each(Roadmap.trackingBars, function(barId) {
                    checkRedeem(barId)
                    .done(function(data) {
                        var linkStatus = data.status;
                        if (linkStatus == 'REDEEM') {
                            removeTracking(barId);
                            callback({
                                barId: barId,
                                pageLink: $.parseJSON(data.pageLink)
                            });
                        } else if (linkStatus == 'UNKNOWN') {
                            removeTracking(barId);
                        }
                        //do nothing with PENDING
                    });
                });
            });
        }

        function removeTracking(barId) {
            Roadmap.trackingBars.splice(Roadmap.trackingBars.indexOf(barId), 1);
        }

        function putLinkToPending(barId) {
            return $.ajax({
                contentType : 'application/json',
                type: "PUT",
                url: AJS.contextPath() + "/rest/roadmap/1.0/bar/" + barId + "/PENDING"
            });
        }

        function checkRedeem(barId) {
            return $.ajax({
                contentType : 'application/json',
                type: "GET",
                url: AJS.contextPath() + "/rest/roadmap/1.0/bar/"+ barId +"/status"
            });
        }

    return {
        addCreateLinkPageListener: goToCreateLinkPage
    };
})(AJS.$, window._);
