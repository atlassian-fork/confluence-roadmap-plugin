(function($, _) {
    var InlineDialogView = Roadmap.InlineDialogView;
    Roadmap.BarDialogView = InlineDialogView.extend({
        events: {
            'click #delete-button': '_onDeleteClick',
            'click .link-new-page': '_goToCreateLinkPage',
            'click .link-update-page': '_goToUpdateLinkPage'
        },

        initialize: function() {
            _.bindAll(this, '_onPageLinkOpen', '_onPageLinkSelectCompleted', '_onPageLinkCallback', '_onPageLinkBlur', '_goToCreateLinkPage');
            this.options.dialogOptions = {
                width: 260,
                timelineWidth: this.options.timelineWidth,
                isSideInlineDialog: true,
                renderOption: this.options.renderOption,
                linkPageEditable: this.options.linkPageEditable,
                createLinkPageCallback: this.options.createLinkPageCallback
            };

            InlineDialogView.prototype.initialize.call(this, this.options);
        },

        /**
         * Get Bar dialog content
         * @param {element} element The inline dialog element.
         * @param {function} showDialog A function that shows the inline dialog.
         */
        _getContent: function(element) {
            element.html(Confluence.Templates.Roadmap.barDialog({
                bar: _.extend({}, this.model.attributes, {
                    description: AJS.escapeHtml(this.model.attributes.description).replace(/\r?\n/g, '<br />')
                }),
                renderOption: this.options.dialogOptions.renderOption,
                linkPageEditable: this.options.dialogOptions.linkPageEditable
            }));
        },

        _onBeforeShow: function() {
            this.fields = this._getFields();

            this._setupInlineEdit();
            this._addEvents();
        },

        _getFields: function() {
            return {
                $title: this.$el.find('#bar-title'),
                $description: this.$el.find('.bar-description'),
                $pageLink: this.$el.find('.bar-page-link'),
                $pageLinkNew: this.$el.find('.link-new-page'),
                $pageLinkUpdate: this.$el.find('.link-update-page')
            };
        },

        _setupInlineEdit: function() {
            var me = this;
            var fields = this.fields;
            var editInplaceOption = this.options.dialogOptions.renderOption.editInplace;
            var defaultOption = {
                form_extra_classes: ['aui'],
                hover_class: 'editable-field-hover aui-icon-small aui-iconfont-edit',
                field_extra_classes: ['text'],
                show_buttons: true,
                save_button: Confluence.Templates.Roadmap.inlineEditSaveButton(),
                cancel_button: Confluence.Templates.Roadmap.inlineEditCancelButton(),
                error_sink: null,
                use_html: true,
                default_text: AJS.I18n.getText('roadmap.editor.new.bar.description'),
                delegate: {
                    shouldOpenEditInPlace: function () {
                        me.$el.find(".inplace_cancel").trigger('click'); // this function will prevent showing two inline edit instances at the same time
                    },
                    didCloseEditInPlace: function() {
                        // edit-inplace still need to rebind original value.
                        // So use setTimeout here to make sure everything was redraw before refresh the inline dialog
                        setTimeout(function() {
                            me.refresh();
                        }, 0);
                    }
                }
            };

            // Setup Title
            editInplaceOption.title && fields.$title.editInPlace(
                $.extend(true, {}, defaultOption, {
                    callback: function(elementId, enteredText) {
                        me.options.updateTitle(enteredText);
                        return AJS.escapeHtml(enteredText);
                    },
                    value_required: true,
                    disable_submit_when_empty: true,
                    delegate: {
                        willOpenEditInPlace: function() {
                            me.refresh();
                            fields.$title.find('.text').attr('maxlength', Roadmap.BarDialog.MAX_LENGTH_TITLE); // set maxlength for model browser
                        }
                    }
                })
            );

            // format italic if description is empty when the dialog show up
            ($.trim(fields.$description.text()) === '') ? fields.$description.addClass('empty-field') : fields.$description.removeClass('empty-field');

            // Setup Description
            var descriptionOption = $.extend(true, {}, defaultOption, {
                field_type: 'textarea',
                field_extra_classes: ['textarea'],
                value_required: false,
                save_if_nothing_changed: true,
                callback: function(elementId, enteredText) {
                    enteredText = $.trim(enteredText);
                    me.options.updateDescription(enteredText);

                    // format italic if description is empty after editing
                    if (enteredText === '') {
                        enteredText = AJS.I18n.getText('roadmap.editor.new.bar.description');
                        $(this).addClass('empty-field');
                    } else {
                        $(this).removeClass('empty-field');
                        Confluence.Roadmap.Analytics.addDescription();
                    }

                    // return the value that will be shown out after editing
                    return AJS.escapeHtml(enteredText).replace(/\r?\n/g, '<br />');
                },
                delegate: {
                    willOpenEditInPlace: function() { // return the value that will be shown in inlineEdit
                        me.refresh();
                        fields.$description.find('.textarea').attr('maxlength', Roadmap.BarDialog.MAX_LENGTH_DESCRIPTION); // set maxlength for model browser
                        return me.model.attributes.description.replace(/<br\s*[\/]?>/gi, '\n');
                    }
                }
            });
            editInplaceOption.description && fields.$description.editInPlace(descriptionOption);

            // Setup Page Link
            var pageLinkOption = $.extend(true, {}, defaultOption, {
                isAutoCompleteField: true,
                show_buttons: false,
                default_text: '',
                field_extra_classes: ['text', 'autocomplete-confluence-content'],
                delegate: {
                    willOpenEditInPlace: me._onPageLinkOpen,
                    didCloseEditInPlace: function() {
                        //hack! when input text is in empty text, there are some cases press enter button do not fire the callback,
                        //therefore the status do not be updated correctly
                        if (_.isEmpty(fields.$pageLink.data('linkData'))) {
                            fields.$pageLink.hide();
                            fields.$pageLinkNew.show();
                            fields.$pageLinkUpdate.show();
                        }
                    }
                },
                on_blur: null,
                callback: me._onPageLinkCallback
            });
            editInplaceOption.pageLink && this.options.dialogOptions.linkPageEditable &&
                this.fields.$pageLink.editInPlace(pageLinkOption);
        },

        _onPageLinkOpen: function(aDOMNode) {
            var me = this;
            aDOMNode.append('<div class="link-page-dropdown" />'); // dropdown list will be rendered here
            var $searchInput = aDOMNode.find('.autocomplete-confluence-content');
            $searchInput.attr({
                placeholder: AJS.I18n.getText('roadmap.editor.dialog.bar.link.placeholder'),
                'data-dropdown-target': '#inline-dialog-roadmap-dialog .link-page-dropdown',
                'data-none-message': AJS.I18n.getText('roadmap.editor.dialog.bar.link.nofound')
            });

            Confluence.Binder.autocompleteConfluenceContent(aDOMNode);
            $searchInput.bind("selected.autocomplete-content", this._onPageLinkSelectCompleted);

            // This blur event will prevent autocomplete field and editinplace edit at the same time
            $searchInput.on('blur', function() {
                setTimeout(me._onPageLinkBlur, 150);
            });

            var pageLink = this.model.attributes.pageLink;
            this.fields.$pageLink.data('linkData', pageLink);
            if (!_.isEmpty(pageLink)) {
                return pageLink.title; // return current page's title
            }
            return true; // return as new search
        },

        _onPageLinkSelectCompleted: function(event, data) {
            event.stopPropagation();

            var $searchInput = this.fields.$pageLink.find('.autocomplete-confluence-content');
            var linkObj = data.content;
            $searchInput.val(linkObj.title);
            this.fields.$pageLink.data('linkData', data.content);

            _.defer(this._onPageLinkBlur);
        },

        _onPageLinkBlur: function() {
            var $searchInput = this.fields.$pageLink.find('.autocomplete-confluence-content');
            var e = $.Event('keyup');
            e.which = AJS.keyCode.ENTER;
            $searchInput.trigger(e);
        },

        _onPageLinkCallback: function(elementId, enteredText) {
            var linkData = (enteredText === '') ? {} : this.fields.$pageLink.data('linkData');
            return this._updatePageLink(linkData);
        },

        _updatePageLink: function(linkData) {
            if (linkData.id === this.model.attributes.pageLink.id) {
                return;
            }

            var fields = this.fields;
            //old linked page ID != new linked page ID
            if (_.isEmpty(linkData)) {
                this.options.updatePageLink({});
                fields.$pageLink.hide();
                fields.$pageLinkNew.show();
                fields.$pageLinkUpdate.show();
                return AJS.I18n.getText('roadmap.editor.dialog.bar.link.empty');
            } else {
                var pageLinkObj = _.pick(linkData, 'id', 'title', 'type', 'spaceKey', 'wikiLink');
                //detect link data come from 'existing link' , it comes from different format
                if (!linkData.spaceKey) {
                    pageLinkObj['spaceKey'] = linkData.space.key;
                }
                this.options.updatePageLink(pageLinkObj);
                fields.$pageLink.show();
                fields.$pageLinkNew.hide();
                fields.$pageLinkUpdate.hide();

                //CONFDEV-31528
                if (this.options.renderOption.isEditMode) {
                    Confluence.Roadmap.Analytics.addPageLinkEditMode({
                        linkedPageId: linkData.id,
                        pageId: AJS.Meta.get('page-id') > 0 ? AJS.Meta.get('page-id') : AJS.Meta.get('draft-id'),
                        isDraft: AJS.Meta.get('page-id') <= 0
                    });
                } else {
                    //blog list doesn't have pageId, we take its URL
                    Confluence.Roadmap.Analytics.addPageLinkViewMode({
                        linkedPageId: linkData.id,
                        pageId: AJS.Meta.get('page-id')
                    });
                }

                return Confluence.Templates.Roadmap.buildPageLink({pageLink: linkData, isEditMode: this.options.renderOption.isEditMode});
            }
        },

        _addEvents: function() {
            // This code bellow will handler limiting max length in for browser which doesn't support maxlength (IE<=9)
            if ($.browser.msie && parseInt($.browser.version, 10) <= 9) {
                var checkLength = function (e, maxLength) {
                    if (e.ctrlKey || e.altKey || e.metaKey || e.keyCode === AJS.keyCode.BACKSPACE || e.keyCode === AJS.keyCode.DELETE) {
                        return true;
                    }
                    var value = $.trim(this.value);

                    return value.length <= maxLength;
                };

                // Keypress
                this.fields.$description.on('keypress', '.textarea', function (e) {
                    return checkLength.call(this, e, Roadmap.BarDialog.MAX_LENGTH_DESCRIPTION);
                });
                // Paste
                this.fields.$description.on('paste', '.textarea', function (e) {
                    var me = this;
                    setTimeout(function() {
                        if (checkLength.call(me, e, Roadmap.BarDialog.MAX_LENGTH_DESCRIPTION)) {
                            me.value = me.value.substr(0, Roadmap.BarDialog.MAX_LENGTH_DESCRIPTION);
                        }
                    }, 0);
                });
            }
        },

        _onDeleteClick: function() {
            this.options.deleteBar();
            this.remove();
        },

        _goToCreateLinkPage: function(event) {
            var dialogOptions = this.options.dialogOptions;
            if (dialogOptions.createLinkPageCallback) {
                var createLinkPageCallback = _.bind(dialogOptions.createLinkPageCallback, {me: this}, event);
                createLinkPageCallback();
            }
        },

        _goToUpdateLinkPage: function(event) {
            event.preventDefault();
            this.fields.$pageLink.show();
            this.fields.$pageLinkNew.hide();
            this.fields.$pageLinkUpdate.hide();
            this.fields.$pageLink.click();
        }
    });
})(AJS.$, window._);