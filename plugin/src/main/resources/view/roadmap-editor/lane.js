(function($, _) {
    window.Roadmap = window.Roadmap || {};
    Roadmap.LaneView = Backbone.View.extend({
        tagName: 'div',
        className: 'roadmap-lane',
        events: {
            "click .roadmap-lane-title": '_showDialog',
            'BarView.drop': '_onBarDropped',
            'BarRowNew.dropover': '_onBarRowNewDropover',
            'BarRowNew.dropout': '_onBarRowNewDropout'
        },

        attributes: function() {
            return {
                cid: this.cid
            };
        },

        initialize: function() {
            _.bindAll(this, 'render', 'renameTitle', 'updateRowIndex', 'removeEmptyRow', 'cleanLaneAfterDrop',
                '_deleteLane', '_updateLaneHeight', '_renderComplete', '_deleteBar', '_addBar', '_changeColor', 'addDefaultBarRow');
            this._barViews = [];
            this._barRowViews = [];

            this.listenTo(this.model.get('bars'), 'add', function(barModel) {
                // New Bar Row
                var rowView = this._newBarRowView();

                // Add Bar to new Row
                var barView = this._newBarView(barModel);
                rowView.$el.append(barView.render().$el);
                this._$laneContent.append(rowView.$el);
                this._barViews.push(barView);
                this.$el.trigger('BarView.add', barView);
                _.defer(this._updateLaneHeight);
            });
        },

        render: function() {
            var me = this;
            this.$el.append(
                Confluence.Templates.Roadmap.lane({
                    title: this.model.get('title'),
                    color: this.model.get('color')
                })
            );
            Confluence.Roadmap.ColorHelper.adjustColorCount(this.model.get('color'), 1);

            var barModels = this.model.get('bars').models;
            barModels = _.sortBy(barModels, function(barModel) {
                return barModel.attributes.rowIndex;
            });

            _.each(barModels, function(barModel) {
                var barView = me._newBarView(barModel);
                me._barViews.push(barView);
            });

            this._renderBars();

            _.defer(this._renderComplete);
            return this;
        },

        _renderComplete: function() {
            this._updateLaneHeight();
        },

        renameTitle: function(newTitle) {
            this.model.set('title', newTitle);
            this.$el.find('.roadmap-lane-title').attr('title', newTitle);
            this.$el.find('.title-inner').html(AJS.escapeHtml(newTitle));
            this._updateLaneHeight();
        },

        _updateLaneHeight: function() {
            //lane-title must be calculated base on 'roadmap-lane-content'
            //To do that: hide title -> get 'roadmap-lane-content' height ->show
            var $titleElement = this.$el.find('.title-inner');
            $titleElement.hide();
            var laneHeight = this._$laneContent.outerHeight();
            $titleElement.width(laneHeight - 2 * Roadmap.LANE_PADDING);
            $titleElement.show();
        },

        _barBeforeResize: function(barViewObj, direction) {
            var barView = barViewObj.$el;
            var paddingBarValue = Math.ceil(barView.outerWidth() - barView.width()); //Control margin/padding value

            var barResizeMaxWidth;
            if (direction == 'left') {
                var barViewLeft = this._getNearestBarView(barViewObj, direction);
                var barViewLeftRightX = barViewLeft == null ? 0 :
                                barViewLeft.position().left + barViewLeft.width() + paddingBarValue;
                var distance = barView.position().left - barViewLeftRightX;
                barResizeMaxWidth = barView.width() + distance;
            } else { //right
                var barViewRight = this._getNearestBarView(barViewObj, direction);
                var barViewRightLeftX = barViewRight == null ?
                                this._getLaneContentWidth() - paddingBarValue :
                                barViewRight.position().left - paddingBarValue;
                barResizeMaxWidth = barViewRightLeftX - barView.position().left;
            }
            barViewObj.updateMaxWidth(barResizeMaxWidth - Roadmap.BAR_MARGIN);
        },

        _renderBars: function() {
            var me = this;
            this._$laneContent = this.$el.find('.roadmap-lane-content');
            var rowViews = {};

            _.each(this._barViews, function(barView) {
                // New Bar Row
                var rowViewKey = 'row-' + barView.model.get('rowIndex');
                var rowView = rowViews[rowViewKey];

                if (!rowView) {
                    rowView = me._newBarRowView(barView.model.get("rowIndex"));
                    rowViews[rowViewKey] = rowView;
                }

                rowView.$el.append(barView.render().$el);
                me._$laneContent.append(rowView.$el);
            });
        },

        _newBarRowView: function() {
            var rowView = new Roadmap.BarRowView();
            this._barRowViews.push(rowView);
            return rowView;
        },

        _newBarView: function(barModel) {
            var barView = new Roadmap.BarView({
                model: barModel,
                lane: this,
                color: this.model.get('color'),
                timelineView: this.options.timelineView,
                deleteBar: this._deleteBar
            });
            barView.on('beforeResize', _.bind(this._barBeforeResize, this)); //listen 'beforeResize' event on Bar
            return barView;
        },

        _getNearestBarView: function(barView, direction) {
            var barViewsInSameRow = _.filter(barView.options.lane._barViews,function(bv){
                return bv.model.get('rowIndex') === barView.model.get('rowIndex');
            });
            var nearBar = null;
            if (direction == 'left') {
                _.each(barViewsInSameRow, function(bar) {
                    if(bar.$el.position().left < barView.$el.position().left
                        && (nearBar == null || bar.$el.position().left > nearBar.$el.position().left)) {
                            nearBar = bar;
                    }
                });
            } else {
                _.each(barViewsInSameRow, function(bar) {
                    if (bar.$el.position().left > barView.$el.position().left
                        && (nearBar == null || bar.$el.position().left < nearBar.$el.position().left)) {
                            nearBar = bar;
                    }
                });
            }

            return nearBar != null ? nearBar.$el : null;
        },

        _getLaneContentWidth: function() {
            return this.$el.find('.roadmap-lane-content').width();
        },

        /**
         * Open the 'Lane' dialog.
         *
         * Called when the 'Lane title' is clicked.
         *
         */
        _showDialog: function() {
            this._laneDialog && this._laneDialog.remove(); // remove existing object if there is any
            this._laneDialog = this._createLaneDialog();
            this._laneDialog.show();
        },

        _createLaneDialog: function() {
            return new Roadmap.LaneDialogView({
                trigger: this.$el.find('.title-outer'),
                canDelete: this.options.canDelete, // provide delete status for lane dialog
                deleteLane: this._deleteLane,
                addBar: this._addBar,
                changeColor: this._changeColor,
                lane: this
            });
        },

        _onBarDropped: function(e, dropInfo, ui) {
            var isAddingNewRow = !!dropInfo.barRowNewView;

            var draggedBarView = ui.draggable.data('View');
            var draggedBarModel = draggedBarView.model;

            var oldLane = draggedBarView.options.lane;
            var inSameLane = (oldLane.cid === this.cid);
            var barCollectionDropped = this.model.get('bars');
            var newLeftPosition = ui.position.left;
            var newStartDate = this.options.timelineView.getBarStartDate(newLeftPosition);

            // don't allow to drop on new row if lane has only one bar
            if (inSameLane && barCollectionDropped.models.length === 1 && isAddingNewRow) {
                return;
            }

            // Update model after drop
            draggedBarView.updatePositionLeft(newLeftPosition);
            draggedBarView.updateColor(this.model.get('color'));
            draggedBarModel.attributes.startDate = newStartDate;

            // Add New Row
            if (isAddingNewRow) {
                var $rowView = this._newBarRowView();
                dropInfo.barRowNewView.$el.replaceWith($rowView.$el);
                draggedBarView.appendTo($rowView.$el);
            }
            // Drag on an existing row
            else {
                draggedBarView.appendTo(dropInfo.$barRow);
            }

            // By default, ui.helper will be removed by jQuery, but we need to remove it manually to get removeEmptyRow() works properly
            ui.helper.remove();

            if (!inSameLane) {
                draggedBarView.options.lane.removeEmptyRow();

                barCollectionDropped._byId[draggedBarModel.id] = draggedBarModel;
                this._barViews.push(draggedBarView);
                // when bar was dragged into new lane need update delete function for new lane can control delete it's bar.
                draggedBarView.updateDeleteBar(this._deleteBar);

                oldLane.model.get('bars').remove(draggedBarModel);
                barCollectionDropped.models.push(draggedBarModel);
                oldLane._barViews.splice(_.indexOf(oldLane._barViews, draggedBarView), 1); //remove draggedBarView from old lane
                this.updateRowIndex(oldLane.$el);
                // Update the default options
                draggedBarView.options = _.extend(draggedBarView.options, {
                    color: this.model.get('color'),
                    lane: this
                });
                oldLane._updateLaneHeight();
            }

            this.removeEmptyRow();

            // Update rowIndex for current lane
            this.updateRowIndex(this.$el);
            this._updateLaneHeight();
        },

        updateRowIndex: function($lane) {
            $lane.find('.roadmap-bar-row').each(function(rowIndex, rowEl) {
                $(rowEl).find('.roadmap-bar:not(".roadmap-bar-clone")').each(function(barIndex, barEl) {
                    var barModel = $(barEl).data('View');
                    if (barModel) {
                        barModel.model.attributes.rowIndex = rowIndex;
                    }
                });
            });
        },

        /**
         * Find and remove empty rows
         *
         * @params {laneView} the lane view is use for find and remove empty rows
         ** @return boolean - true if has empty row is removed
         */
        removeEmptyRow: function() {
            var barRowDeleted = 0;
            _.each(this._barRowViews, function(barRow) {
                if (barRow.isEmptyRow()) {
                    barRow.remove();
                    barRowDeleted++;
                }
            });

            return !!barRowDeleted;
        },

        // this function will clear all stuff that generated by D&D and should be call when dragging a bar was stopped.
        cleanLaneAfterDrop: function() {
            this._$laneContent.find('.roadmap-bar-drag-active').removeClass('roadmap-bar-drag-active');
            this._$laneContent.find('.new-row-placeholder').remove();
        },

        _deleteLane: function() {
            Confluence.Roadmap.ColorHelper.adjustColorCount(this.model.get('color'), -1);
            this.$el.trigger( 'Lane.delete', this );
        },

        /**
         * Update delete status show/hide delete button for this lane when open lane dialog
         *
         * @param {canDelete} true show delete button
         */
        updateDeleteStatus: function(canDelete) {
            if(this._laneDialog) {
                this._laneDialog.remove(); // remove previous dialog
                delete this._laneDialog;
            }
            this.options.canDelete = canDelete;
        },

        _deleteBar: function(barView) {
            this.model.get('bars').remove(barView.model);
            this._barViews.splice(_.indexOf(this._barViews, barView), 1);
            barView.remove();
            if (this.removeEmptyRow()) {
                this.updateRowIndex(this.$el);
                this._updateLaneHeight();
                this.$el.trigger('BarRowView.remove');
            }
        },

        _addBar: function() {
            var bars = this.model.get('bars');

            var timeline = this.options.timelineView.model;
            var mStartDateTimeline = moment(timeline.get('startDate'));
            var mStartDateToRender = timeline.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
                    ? mStartDateTimeline.startOf('month')
                    : mStartDateTimeline.startOf('isoWeek');

            bars.add(new Roadmap.Bar({
                rowIndex: this.model.getNumberOfRows(),
                startDate: mStartDateToRender.toDate()
            }));
        },

        _changeColor: function(newColor) {
            Confluence.Roadmap.ColorHelper.adjustColorCount(this.model.get('color'), -1);
            this.model.set('color', newColor);
            Confluence.Roadmap.ColorHelper.adjustColorCount(newColor, 1);
            this.$el.find('.roadmap-lane-title').css({
                'background-color': newColor.lane,
                color: newColor.text
            });
            _.each(this._barViews, function(barView) {
                barView.updateColor(newColor);
            });
        },

        addNewBarRow: function($draggingBar) {
            var $barRows = this._$laneContent.find('.roadmap-bar-row');
            var barRowNewViewList = [];

            var add = function() {
                $barRows.each(function () {
                    var $barRow = $(this);
                    var isFirstRow = $barRow.index() === 0;

                    if (isFirstRow) {
                        var barRowNewViewBefore = new Roadmap.BarRowNewView({
                            $barRow: $barRow,
                            renderBottom: false
                        });
                        $barRow.before(barRowNewViewBefore.render().$el);
                        barRowNewViewList.push(barRowNewViewBefore);
                    }

                    var barRowNewViewAfter = new Roadmap.BarRowNewView({
                        $barRow: $barRow
                    });
                    $barRow.after(barRowNewViewAfter.render().$el);
                    barRowNewViewList.push(barRowNewViewAfter);
                });
            };

            if ($barRows.length == 0) { // Empty lane
                this.addDefaultBarRow();
            } else {
                var $bars = $barRows.find('.roadmap-bar');
                if ($bars.length > 0) { // this is empty lane with default bar-row
                    var selfDropped = ($bars.length > 0) && ($bars.length == 1) && ($bars.attr('cid') === $draggingBar.attr('cid'));  // Drop the bar into itself
                    if (!selfDropped) {
                        add();
                    }
                }
            }

            return barRowNewViewList;
        },

        /**
         * Add a empty row as default if lane is empty
         */
        addDefaultBarRow: function() {
            var rowView = this._newBarRowView();
            this._$laneContent.append(rowView.$el);
        },

        _onBarRowNewDropover: function(e, barRowNew, ui) {
            if (!_.isEmpty(Roadmap.DragDrop.barDraggingOver)) {
                // Make sure there is only one bar-row-new activated at a time
                _.each(Roadmap.DragDrop.barDraggingOver, function(item) {
                    item.$el && item.$el.removeClass('new-bar-row-hover roadmap-bar-drag-hover');
                });
            }

            Roadmap.DragDrop.barDraggingOver[barRowNew.cid] = barRowNew;
            ui.helper.removeClass('roadmap-bar-overlapped');
            ui.helper.data('isAddingNewRow', true);
        },

        _onBarRowNewDropout: function(e, barRowNew, ui) {
            ui.helper.data('isAddingNewRow', false);
            delete Roadmap.DragDrop.barDraggingOver[barRowNew.cid];

            _.each(Roadmap.DragDrop.barDraggingOver, function(item) {
                if (typeof item === 'string') { // still over on a bar
                    ui.helper.addClass('roadmap-bar-overlapped');
                } else if (item.className === 'roadmap-bar-row' && ui.helper.data('isAddingNewRow') !== true) { // still in a bar-row
                    item.$el.addClass('roadmap-bar-drag-hover');
                }
            });
        }
    });
})(AJS.$, window._);