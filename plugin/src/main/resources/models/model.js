(function() {
    window.Roadmap = window.Roadmap || {};
    Roadmap.Bar = Backbone.Model.extend({
        defaults: function() {
            return {
                id: Confluence.Roadmap.Helper.guid(),
                title: AJS.I18n.getText('roadmap.editor.new.bar.text'),
                description: '',
                startDate: new Date(),
                duration: 1,
                rowIndex: 0,
                pageLink: {}
            };
        }
    });

    Roadmap.Lane = Backbone.Model.extend({
        defaults: function() {
            return {
                title: AJS.I18n.getText('roadmap.editor.new.lane.text'),
                color: Confluence.Roadmap.ColorHelper.getColor(),
                bars: []
            };
        },
        initialize: function() {
            var bars = new Roadmap.Bars(this.get('bars'));
            this.set('bars', bars);
        },
        getNumberOfRows: function() {
            var bars = this.get('bars');

            if (bars.length === 0) { // in case lane is empty
                return 0;
            }

            var totalRows = 0;
            bars.each(function(bar) {
                totalRows = Math.max(bar.get('rowIndex'), totalRows);
            });
            return totalRows + 1;
        }
    });

    Roadmap.Marker = Backbone.Model.extend({
        defaults: function() {
            return {
                title: AJS.I18n.getText('roadmap.editor.default.marker.title')
            };
        }
    });

    Roadmap.Timeline = Backbone.Model.extend({
        defaults: function() {
            return {
                displayOption: Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
            };
        },

        validate: function(attrs) {
            var errors = [];

            var mStartDate = Confluence.Roadmap.DateUtilities.parseDate(attrs.startDateString);
            var mEndDate = Confluence.Roadmap.DateUtilities.parseDate(attrs.endDateString);

            if (!mStartDate || !mEndDate) {
                errors.push({name: 'date', message: AJS.I18n.getText('roadmap.editor.error.field.date')});
            } else if (mStartDate.diff(mEndDate) >= 0) {
                // Start Date and End Date are valid, validate Start Date < End Date
                errors.push({name: 'endDate', message: AJS.I18n.getText('roadmap.editor.error.field.enddate')});
            } else if (mEndDate.diff(mStartDate, 'years', true) > Roadmap.TIMELINE_YEARS_LIMIT) {
                errors.push({name: 'endDate', message: AJS.I18n.getText('roadmap.editor.error.timeline', Roadmap.TIMELINE_YEARS_LIMIT)});
            }

            return errors.length > 0 ? errors : false;
        },

        calculateTimelineColumns: function() {
            var startDate = this.get('startDate');
            var endDate = this.get('endDate');
            var displayTimelineColumns = [];
            var mDate;

            if (this.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                mDate = moment(startDate).clone().startOf('month');
                var endDateMonth = moment(endDate).clone().endOf('month');

                do {
                    displayTimelineColumns.push({
                        month: Confluence.Roadmap.DateUtilities.getDateStringFromDate(mDate, Roadmap.MONTH_FORMAT),
                        year: mDate.get('year')
                    });
                    mDate.add(1, 'months');
                } while (mDate.diff(endDateMonth) < 0);

            } else {
                mDate = moment(startDate).clone().startOf('isoWeek');
                var endDateWeek = moment(endDate).clone().endOf('isoWeek');

                do {
                    displayTimelineColumns.push({
                        month: Confluence.Roadmap.DateUtilities.getDateStringFromDate(mDate, Roadmap.WEEK_FORMAT),
                        year: mDate.get('year')
                    });
                    mDate.add(1, 'weeks');
                } while (mDate.diff(endDateWeek) < 0);
            }

            // avoid using 'set' method to avoid triggering 'change` event on timeline module.
            this.attributes.displayTimelineColumns = displayTimelineColumns;
        }
    });

    Roadmap.Roadmap = Backbone.Model.extend({
        defaults: function() {
            return {
                title: AJS.I18n.getText('roadmap.editor.title'),
                lanes: [],
                markers: []
            };
        },
        initialize: function() {
            var lanes = new Roadmap.Lanes(this.get('lanes'));
            this.set('lanes', lanes);

            var markers = new Roadmap.Markers(this.get('markers'));
            this.set('markers', markers);

            var timeline = new Roadmap.Timeline(this.get('timeline'));
            this.set('timeline', timeline);
        },

        parse: function(roadmapData) {
            if(!roadmapData.timeline) {
                return Roadmap.getDefaultRoadmapData();
            }
            return this.fromMacroData(roadmapData);
        },

        /**
         * convert roadmap macro json from server to model
         *
         * @param {roadmap} date string with Roadmap date format
         * @return roadmap data for model
         */
        fromMacroData: function(roadmap) {
            var timeline = roadmap.timeline;
            timeline.startDate = Confluence.Roadmap.DateUtilities.getDateFromDateString(timeline.startDate);
            timeline.endDate = Confluence.Roadmap.DateUtilities.getDateFromDateString(timeline.endDate);

            var lanes = roadmap.lanes;
            _.each(lanes, function(lane) {
                var bars = lane.bars;
                _.each(bars, function(bar) {
                    bar.startDate = Confluence.Roadmap.DateUtilities.getDateFromDateString(bar.startDate);
                    if (!bar.id)
                    {
                        bar.id = Confluence.Roadmap.Helper.guid();
                    }
                });
                lane.bars = bars;
            });
            roadmap.lanes = lanes;

            var markers = roadmap.markers;
            _.each(markers, function(marker) {
                marker.markerDate = Confluence.Roadmap.DateUtilities.getDateFromDateString(marker.markerDate);
            });
            roadmap.markers = markers;

            return roadmap;
        },

        /**
         * convert backbone model to macro data
         *
         * @return roadmap macro data
         */
        toMacroData: function() {
            var roadmap = this.toJSON();
            var timeline = roadmap.timeline.toJSON();
            timeline.startDate = Confluence.Roadmap.DateUtilities.getDateStringFromDate(timeline.startDate);
            timeline.endDate = Confluence.Roadmap.DateUtilities.getDateStringFromDate(timeline.endDate);
            delete timeline.displayTimelineColumns;
            roadmap.timeline = timeline;

            var lanes = roadmap.lanes.toJSON();
            _.each(lanes, function(lane) {
                var bars = lane.bars.toJSON();
                _.each(bars, function(bar) {
                    bar.startDate = Confluence.Roadmap.DateUtilities.getDateStringFromDate(bar.startDate);
                });
                lane.bars = bars;
            });
            roadmap.lanes = lanes;

            var markers = roadmap.markers.toJSON();
            _.each(markers, function(marker) {
                marker.markerDate = Confluence.Roadmap.DateUtilities.getDateStringFromDate(marker.markerDate);
            });
            roadmap.markers = markers;

            return roadmap;
        },

        updateDurationUnit: function(displayOption) {
            this.get('lanes').each(function(laneModel) {
                laneModel.get('bars').each(function(bar) {
                    var duration;
                    duration = displayOption === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
                            ? Confluence.Roadmap.DateUtilities.convertToMonthDuration(bar.get('startDate'), bar.get('duration'))
                            : Confluence.Roadmap.DateUtilities.convertToWeekDuration(bar.get('startDate'), bar.get('duration'));

                    bar.attributes.duration = duration;
                });
            });
        }
    });
})();