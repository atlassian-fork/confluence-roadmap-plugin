package com.atlassian.plugins.roadmap.renderer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;

import javax.imageio.ImageIO;

import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.plugins.roadmap.renderer.enricher.NoopInfoEnricher;

import org.apache.commons.codec.binary.Base64;

/**
 * Implementation for {AbstractRoadmapRenderer} to convert from java2D to png image
 */
public class PNGRoadMapRenderer extends AbstractTimelinePlannerRenderer
{
    private BufferedImage img;
    private Graphics2D graphics2D;

    public BufferedImage renderAsImage(TimelinePlanner roadmap, Optional<Integer> widthOption,
                                       Optional<Integer> heightOption, boolean isPlaceHolder) throws IOException
    {
        this.drawImage(roadmap, widthOption, heightOption, isPlaceHolder);
        this.graphics2D.dispose();
        this.graphics2D = null;
        BufferedImage ret = img;
        this.img = null;
        return ret;
    }

    public byte[] renderAsBytes(TimelinePlanner roadmap) throws IOException
    {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        ImageIO.write(renderAsImage(roadmap, Optional.empty(), Optional.empty(), false), "png", buffer);
        return buffer.toByteArray();
    }

    public String renderAsBase64(TimelinePlanner roadmap) throws IOException
    {
        return Base64.encodeBase64String(renderAsBytes(roadmap));
    }

    @Override
    protected Graphics2D createDummyGraphics2D()
    {
        BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        return img.createGraphics();
    }

    @Override
    protected Graphics2D createGraphics2D(int width, int height)
    {
        this.img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.graphics2D = img.createGraphics();
        return this.graphics2D;
    }

    @Override
    protected RenderedImageInfoEnricher createEnricher()
    {
        return new NoopInfoEnricher();
    }
}
