package com.atlassian.plugins.roadmap.models;

import java.util.Date;

public class Bar
{
    private String id;
    private String title;
    private String description;
    private Date startDate;
    private double duration;
    private int rowIndex;
    private RoadmapPageLink pageLink;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public double getDuration()
    {
        return duration;
    }

    public void setDuration(double duration)
    {
        this.duration = duration;
    }

    public int getRowIndex()
    {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex)
    {
        this.rowIndex = rowIndex;
    }

    public RoadmapPageLink getPageLink()
    {
        return pageLink;
    }

    public void setPageLink(RoadmapPageLink pageLink)
    {
        this.pageLink = pageLink;
    }
}
