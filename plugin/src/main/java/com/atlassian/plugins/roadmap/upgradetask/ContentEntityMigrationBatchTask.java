package com.atlassian.plugins.roadmap.upgradetask;

import com.atlassian.confluence.content.render.xhtml.migration.BatchException;
import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.content.render.xhtml.migration.ExceptionTolerantMigrator;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
class ContentEntityMigrationBatchTask implements BatchTask<ContentEntityObject>
{
    private static Logger log = LoggerFactory.getLogger(ContentEntityMigrationBatchTask.class);

    private final ExceptionTolerantMigrator migrator;
    private final ContentEntityManager entityManager;

    public ContentEntityMigrationBatchTask(ExceptionTolerantMigrator migrator, ContentEntityManager entityManager)
    {
        this.migrator = migrator;
        this.entityManager = entityManager;
    }

    @Override
    public boolean apply(final ContentEntityObject entity, final int index, final int batchSize)
            throws BatchException, CloneNotSupportedException
    {
        final List<RuntimeException> exceptions = new ArrayList<RuntimeException>();
        boolean migrateResult = false;

        try
        {
            migrateResult = Helper.migrate(entity, migrator, entityManager);
        }
        catch (RuntimeException e)
        {
            exceptions.add(e);
        }

        if (!exceptions.isEmpty())
        {
            throw new BatchException(exceptions);
        }
        return migrateResult;
    }
}
