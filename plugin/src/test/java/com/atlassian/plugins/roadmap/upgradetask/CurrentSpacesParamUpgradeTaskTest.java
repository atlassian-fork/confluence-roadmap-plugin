package com.atlassian.plugins.roadmap.upgradetask;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class CurrentSpacesParamUpgradeTaskTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CurrentSpacesParamMigrator migrator;

    @Test
    public void testDoUpgrade() throws Exception
    {
        CurrentSpacesParamUpgradeTask upgradeTask = new CurrentSpacesParamUpgradeTask(migrator);
        assertEquals(6, upgradeTask.getBuildNumber());

        upgradeTask.doUpgrade();
        verify(migrator).migrate();
    }
}