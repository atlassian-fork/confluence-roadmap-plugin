(function() {
    /* global QUnit:false */
    'use strict';

    QUnit.module('Roadmap utilities', {
        beforeEach: function() {
        },

        afterEach: function() {
        }
    });

    QUnit.test('test "parseDate" method', function() {
        var parseDate = Confluence.Roadmap.DateUtilities.parseDate;

        var strDate = '2015-13-01';
        var mDate = parseDate(strDate);
        QUnit.assert.strictEqual(mDate, null);

        var strDate = '2015-1-01';
        var mDate = parseDate(strDate);
        QUnit.assert.strictEqual(mDate, null);

        strDate = '';
        mDate = parseDate(strDate);
        QUnit.assert.strictEqual(mDate, null);

        var strDate = '2015-12-01';
        mDate = parseDate(strDate);
        QUnit.assert.ok(mDate.diff(moment(strDate, 'YYYY-MM-DD')) === 0);

        var strDate = '15-12-01';
        mDate = parseDate(strDate);
        QUnit.assert.ok(mDate.diff(moment(strDate, 'YY-MM-DD')) === 0);
    });

    QUnit.test('test "convertTimeline" method', function() {
        var parseDate = Confluence.Roadmap.DateUtilities.parseDate;
        var convertToWeekDuration = Confluence.Roadmap.DateUtilities.convertToWeekDuration;
        var convertToMonthDuration = Confluence.Roadmap.DateUtilities.convertToMonthDuration;

        var strDate = '2015-01-14';
        var monthDuration = 0.85;

        var mStartDate = parseDate(strDate);
        var weekDuration = convertToWeekDuration(mStartDate.toDate(), monthDuration);
        var convertMonthDuration = convertToMonthDuration(mStartDate.toDate(), weekDuration);
        QUnit.assert.equal(convertMonthDuration.toFixed(2), monthDuration);

        monthDuration = 1.25;
        mStartDate = parseDate(strDate);
        weekDuration = convertToWeekDuration(mStartDate.toDate(), monthDuration);
        convertMonthDuration = convertToMonthDuration(mStartDate.toDate(), weekDuration);
        QUnit.assert.equal(convertMonthDuration.toFixed(2), monthDuration);

        monthDuration = 1.85;
        mStartDate = parseDate(strDate);
        weekDuration = convertToWeekDuration(mStartDate.toDate(), monthDuration);
        convertMonthDuration = convertToMonthDuration(mStartDate.toDate(), weekDuration);
        QUnit.assert.equal(convertMonthDuration.toFixed(2), monthDuration);

        monthDuration = 2.25;
        mStartDate = parseDate(strDate);
        weekDuration = convertToWeekDuration(mStartDate.toDate(), monthDuration);
        convertMonthDuration = convertToMonthDuration(mStartDate.toDate(), weekDuration);
        QUnit.assert.equal(convertMonthDuration.toFixed(2), monthDuration);
    });

}());
