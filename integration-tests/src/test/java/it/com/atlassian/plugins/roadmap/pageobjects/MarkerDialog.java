package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class MarkerDialog extends Dialog
{
    @ElementBy(id = "confluence-roadmap-dialog")
    private PageElement roadmapEditorDialog;

    @ElementBy(id = "rename-button")
    private PageElement renameButton;

    @ElementBy(id = "delete-button")
    private PageElement deleteButton;

    public MarkerDialog()
    {
        super("inline-dialog-roadmap-dialog");
    }

    public MarkerRenameDialog clickRename()
    {
        renameButton.click();
        return this.pageBinder.bind(MarkerRenameDialog.class);
    }

    public void clickDelete()
    {
        deleteButton.click();
    }
}
